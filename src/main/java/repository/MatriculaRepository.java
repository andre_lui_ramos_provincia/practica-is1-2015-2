
package repository;

import java.util.Collection;

import domain.Matricula;

public interface MatriculaRepository extends BaseRepository<Matricula, Long> {
	Matricula findByNumber(String number);

	Collection<Matricula> findByPersonId(Long personId);
}