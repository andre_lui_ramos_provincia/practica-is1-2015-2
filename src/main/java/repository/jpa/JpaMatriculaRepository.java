package repository.jpa;



import java.util.Collection;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import repository.MatriculaRepository;
import domain.Matricula;

@Repository
public class JpaMatriculaRepository extends JpaBaseRepository<Matricula, Long> implements
		MatriculaRepository {

	@Override
	public Matricula findByNumber(String number) {
		//SELECT a.id, a.number, a.date FROM tbl_account a WHERE a.number = :number
		String jpaQuery = "SELECT a FROM Account a WHERE a.number = :number";
		TypedQuery<Matricula> query = entityManager.createQuery(jpaQuery, Matricula.class);
		query.setParameter("number", number);
		return getFirstResult(query);
	}

	@Override
	public Collection<Matricula> findByPersonId(Long personId) {
		String jpaQuery = "SELECT a FROM Account a JOIN a.owners p WHERE p.id = :personId";
		TypedQuery<Matricula> query = entityManager.createQuery(jpaQuery, Matricula.class);
		query.setParameter("personId", personId);
		return query.getResultList();
	}
}
