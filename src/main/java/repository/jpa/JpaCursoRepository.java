package repository.jpa;



import java.util.Collection;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import repository.CursoRepository;
import domain.Curso;

@Repository
public class JpaCursoRepository extends JpaBaseRepository<Curso, Long> implements
		CursoRepository {

	@Override
	public Curso findByNumber(String number) {
		//SELECT a.id, a.number, a.date FROM tbl_account a WHERE a.number = :number
		String jpaQuery = "SELECT a FROM Account a WHERE a.number = :number";
		TypedQuery<Curso> query = entityManager.createQuery(jpaQuery, Curso.class);
		query.setParameter("number", number);
		return getFirstResult(query);
	}

	@Override
	public Collection<Curso> findByPersonId(Long personId) {
		String jpaQuery = "SELECT a FROM Account a JOIN a.owners p WHERE p.id = :personId";
		TypedQuery<Curso> query = entityManager.createQuery(jpaQuery, Curso.class);
		query.setParameter("personId", personId);
		return query.getResultList();
	}
}
