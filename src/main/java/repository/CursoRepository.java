
package repository;

import java.util.Collection;

import domain.Curso;

public interface CursoRepository extends BaseRepository<Curso, Long> {
	Curso findByNumber(String number);

	Collection<Curso> findByPersonId(Long personId);
}