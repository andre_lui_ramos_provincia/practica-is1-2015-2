package domain;

import java.util.Collection;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;


@Entity
public class Curso implements BaseEntity<Long> {
	@Id
	@SequenceGenerator(name = "curso_id_generator", sequenceName = "curso_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "curso_id_generator")
	private Long id;

	@Column(length = 64)
	private String codigo;

	@Column(length = 64)
	private String nombre;

	@Column(length = 64)
	private Integer creditos;

	//@Column
	//private List<Curso> prerequisitos;

	@OneToMany(mappedBy = "sourceCurso",fetch = FetchType.LAZY)
	private Collection<Matricula> foragenCurso;
	
	

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getCreditos() {
		return creditos;
	}

	public void setCreditos(Integer creditos) {
		this.creditos = creditos;
	}

	//public List<Curso> getPrerequisitos() {
		//return prerequisitos;
	//}

	//public void setPrerequisitos(List<Curso> prerequisitos) {
		//this.prerequisitos = prerequisitos;
	//}

}
